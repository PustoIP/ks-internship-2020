<?php

use Laravel\Lumen\Routing\Router;

/**
 * @var Router $router
 */

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => '/api/v1'], function () use ($router) {
    $router->group(['prefix' => '/auth'], function () use ($router) {
        $router->get('/login', 'AuthController@login');
        $router->post('/register', 'AuthController@register');

    });
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->post('/auth/confirm-register/{apiToken}', 'AuthController@confirmRegister');
        $router->group(['prefix' => '/user'], function () use ($router) {
            $router->put('/password', 'AuthController@updatePassword');
        });
        $router->group(['prefix' => '/directory'], function () use ($router) {
            $router->post('/create', 'DirectoriesController@createAction');
            $router->patch('/{directoryUuid}/update', 'DirectoriesController@updateAction');
            $router->get('/', 'DirectoriesController@listAction');
            $router->get('/{directoryUuid}', 'DirectoriesController@getAction');
        });
        $router->group(['prefix' => '/directory/{directoryUuid}/files'], function () use ($router) {
            $router->post('/', 'FilesController@uploadAction');
            $router->delete('/{fileUuid}', 'FilesController@deleteAction');
            $router->get('/', 'FilesController@listAction');
            $router->put('/{fileUuid}', 'FilesController@editAction');
            $router->post('/{fileUuid}/move', 'FilesController@moveAction');
            $router->post('/{fileUuid}/copy', 'FilesController@copyAction');
            $router->get('/{fileUuid}', 'FilesController@downloadAction');
        });
        $router->group(['prefix' => '/invites'], function () use ($router) {
            $router->post('/', 'InvitesController@confirmAction');
        });
        $router->group(['prefix' => '/directories/{directoryUuid}/shares'], function () use ($router) {
            $router->post('/', 'InvitesController@shareAction');
            $router->delete('/{inviteId}', 'InvitesController@deleteAction');
            $router->put('/', 'InvitesController@leaveAction');

        });
    });

});
