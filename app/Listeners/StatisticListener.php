<?php


namespace App\Listeners;


use App\Events\StatisticQueueEvent;
use App\Services\QueueService;

/**
 * Class StatisticListener
 * @package App\Listeners
 */
abstract class StatisticListener
{
    /**
     * @var QueueService
     */
    protected $service;

    /**
     * StatisticListener constructor.
     * @param QueueService $service
     */
    public function __construct(QueueService $service)
    {
        $this->service = $service;
    }

    /**
     * @param StatisticQueueEvent $event
     */
    public function handle(StatisticQueueEvent $event): void
    {
        $this->service->sendMessage($event->getDataArray());
    }
}
