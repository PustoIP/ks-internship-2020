<?php


namespace App\Services;

use App\Events\FileAddedEvent;
use App\Exceptions\TotalFilesLimitException;
use App\Models\Directory;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use App\Models\File as FileModel;
use Throwable;


/**
 * Class FilesServices
 * @package App\Services
 */
class FilesServices
{
    /**
     * @var mixed
     */
    private $maxLifetimeSeconds;
    private $confirmedAccMaxSize;
    private $nonConfirmedAccMaxSize;
    private $filesDirectory;
    /**
     * @var int $lifeTime
     */
    private $lifeTime;

    /**
     * FilesServices constructor.
     */
    public function __construct()
    {
        $config = config('exchanger');
        $this->maxLifetimeSeconds = $config['file_max_lifetime_sec'];
        $this->confirmedAccMaxSize = $config['confirmed_account_max_size'];
        $this->nonConfirmedAccMaxSize = $config['not_confirmed_account_max_size'];
        $this->filesDirectory = $config['files_directory'];

    }

    /**
     * @param User $user
     * @param Directory $directory
     * @param UploadedFile $file
     * @param string $fileName
     * @param int|null $lifeTimeSeconds
     * @throws Throwable
     */
    public function saveFile(User $user,
                             Directory $directory,
                             UploadedFile $file,
                             string $fileName,
                             int $lifeTimeSeconds = null): void
    {
        $fileSizeKb = intdiv($file->getSize(), 1024);
        $this->checkUserMaxSize($user, $fileSizeKb);
        $this->checkLifeTimeSeconds($lifeTimeSeconds);

        $fileModel = new FileModel([
            'name' => $fileName,
            'uuid' => Str::uuid(),
            'filename' => $file->getClientOriginalName(),
            'lifetime_seconds' => $this->lifeTime,
            'file_size' => $fileSizeKb,
            'created_at' => Carbon::now()
        ]);
        $fileModel->saveOrFail();
        $directory->files()->attach($fileModel->id);

        $file->move($this->filesDirectory, $file->getClientOriginalName());

        $user->raiseFileSize($fileSizeKb);
        event(new FileAddedEvent($user,$directory,$fileModel));
    }

    /**
     * @param User $user
     * @param int $newFileSize
     * @return JsonResponse|void
     * @throws TotalFilesLimitException
     */
    private function checkUserMaxSize(User $user, int $newFileSize)
    {
        $currentSize = $user->getCurrentFilesSize();
        $newTotalSize = $currentSize + $newFileSize;

        if ($user->confirmed && $newTotalSize < $this->confirmedAccMaxSize) {
            return;
        }

        if (!$user->confirmed && $newTotalSize < $this->nonConfirmedAccMaxSize) {
            return;
        }
        throw new TotalFilesLimitException('File total size limit!');
    }

    /**
     * @param int|null $lifeTimeSeconds
     * @return int|mixed
     */
    private function checkLifeTimeSeconds(?int $lifeTimeSeconds)
    {
        if (is_null($lifeTimeSeconds)) {
            return $this->lifeTime = $this->maxLifetimeSeconds;
        }
        return $this->lifeTime = $lifeTimeSeconds;
    }
}
