<?php


namespace App\Services;


use App\Events\DirectorySharedEvent;
use App\Events\UserInviteEvent;
use App\Exceptions\InvitesLimitException;
use App\Exceptions\UserConfirmException;
use App\Mail\SendMail;
use App\Models\Directory;
use App\Models\Invite;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class InvitesServices
 * @package App\Services
 */
class InvitesServices
{

    /**
     * @var mixed
     */
    private $maxUniqDirectories;
    /**
     * @var SendMail
     */
    private $mail;
    /**
     * InvitesServices constructor.
     */
    public function __construct()
    {
        $this->mail = new SendMail();
        $config = config('exchanger');
        $this->maxUniqDirectories = $config['max_uniq_shared_directories'];
    }

    /**
     * @param User $owner
     * @param Directory $directory
     * @param string $inviteEmail
     * @return Invite|Exception|Throwable
     * @throws InvitesLimitException
     * @throws UserConfirmException
     */
    public function shareInvite(User $owner, Directory $directory, string $inviteEmail)
    {
        $this->checkUserConfirm($owner);
        $this->checkUserInvites($directory,$inviteEmail);
        $this->checkInvitesCounter($owner,$directory);
        DB::beginTransaction();
        try {
            $invite = new Invite([
                'token' => Str::random(),
                'inviter_id' => $owner->id,
                'invite_email' => $inviteEmail,
                'created_at' => Carbon::now()
            ]);
            $invite->saveOrFail();
            $this->raiseInviteCounter($directory,$owner);
            $invite->directories()->attach($directory->id);
        } catch (Throwable $throwable){
            DB::rollBack();
            return $throwable;
        }
        DB::commit();
        $user = User::query()->where('email',$inviteEmail)->first();
        if ($user === null){
           $this->mail->sendInviteMail($inviteEmail);
           $user = new User(['email' => $inviteEmail]);
           event(new UserInviteEvent($owner,$inviteEmail));
        }
        event(new DirectorySharedEvent($owner,$user,$directory));
        return $invite;
    }

    /**
     * @param User $user
     * @throws UserConfirmException
     */
    public function checkUserConfirm(User $user): void
    {
        if ($user->confirmed){
            return;
        }
        throw new UserConfirmException('User not confirmed!');
    }

    /**
     * @param Directory $directory
     * @param string $inviteEmail
     * @throws InvitesLimitException
     */
    public function checkUserInvites(Directory $directory, string $inviteEmail): void
    {
        $invite = $directory->invites()->where('invite_email',$inviteEmail)->exists();
        if ($invite === false){
            return;
        }
        throw new InvitesLimitException('You have already sent this invitation!');
    }

    /**
     * @param User $user
     * @param Directory $directory
     * @throws InvitesLimitException
     */
    public function checkInvitesCounter(User $user, Directory $directory): void
    {
        $counter = $user->getInviteCounter();
        if ($counter < $this->maxUniqDirectories){
            return;
        }
        if ($counter = $this->maxUniqDirectories){
            $bool = $directory->invites()->exists();
            if ($bool === true){
                return;
            }
        }
        throw new InvitesLimitException('You can share no more than 5 unique directories!');
    }

    /**
     * @param Directory $directory
     * @return bool
     */
    public function checkUniqDirectory(Directory $directory): bool
    {
        return $directory->invites()->exists();
    }

    /**
     * @param Directory $directory
     * @param User $user
     */
    public function raiseInviteCounter(Directory $directory, User $user): void
    {
        $uniqDirectory = $this->checkUniqDirectory($directory);
        if ($uniqDirectory === true){
            return;
        }
        $user->incrementInviteCounter(true);
    }

    /**
     * @param Directory $directory
     * @param User $user
     */
    public function downgradeInviteCounter(Directory $directory, User $user): void
    {
        $uniqDirectory = $this->checkUniqDirectory($directory);
        if ($uniqDirectory === true){
            return;
        }
        $user->incrementInviteCounter(false);
    }

}
