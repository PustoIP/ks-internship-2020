<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Invite
 * @package App\Models
 */
class Invite extends Model
{
    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'inviter_id','id');
    }

    /**
     * @return BelongsToMany
     */
    public function directories(): BelongsToMany
    {
        return $this
            ->belongsToMany(Directory::class, 'invites_shared_directories', 'invites_id', 'directories_id');
    }


    /**
     * @var string
     */
    protected $table = 'invites';
    /**
     * @var array
     */
    protected $fillable = ['token','inviter_id','invite_email','created_at'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
