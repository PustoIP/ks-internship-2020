<?php


namespace App\Models;



use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;



/**
 * Class File
 * @package App\Models
 */
class File extends Model
{
    /**
     * @var string
     */
    protected $table = 'files';
    /**
     * @var string[]
     */
    protected $fillable = [
      'filename',
      'name',
      'uuid',
      'lifetime_seconds',
      'file_size',
      'created_at'
    ];

    /**
     * @return BelongsToMany
     */
    public function directories(): BelongsToMany
    {
        return $this
            ->belongsToMany(Directory::class, 'directories_files', 'files_id', 'directories_id');
    }

    /**
     * @var bool
     */
    public $timestamps = false;

}
