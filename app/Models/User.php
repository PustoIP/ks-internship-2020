<?php

namespace App\Models;


use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Redis;
use Laravel\Lumen\Auth\Authorizable;



/**
 * @method static where(string $string, $input)
 * @method static create(array $array)
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;


    public const TOTAL_FILES_CACHE_PREFIX = 'Exchanger::totalFiles::';
    public const TOTAL_INVITES_CACHE_PREFIX = 'Exchanger::totalInvites::';

    /**
     * @return HasMany
     */
    public function invites(): HasMany
    {
        return $this->hasMany(Invite::class);
    }
    /**
     * @return BelongsToMany
     */
    public function directories(): BelongsToMany
    {
        return $this
            ->belongsToMany(Directory::class,'users_directories', 'users_id','directories_id')
            ->withPivot('is_master');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'user_password_hash','api_token','created_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_password_hash'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return int
     */
    public function getCurrentFilesSize(): int
    {
        return (int)Redis::connection()->client()->get(self::TOTAL_FILES_CACHE_PREFIX . $this->id);
    }

    /**
     * @param int $fileSize
     */
    public function raiseFileSize(int $fileSize): void
    {
        Redis::connection()->client()->incrBy(self::TOTAL_FILES_CACHE_PREFIX . $this->id, $fileSize);
    }

    /**
     * @param int $fileSize
     */
    public function downgradeFileSize(int $fileSize): void
    {
        Redis::connection()->client()->decrBy(self::TOTAL_FILES_CACHE_PREFIX . $this->id, $fileSize);
    }

    /**
     * @return int
     */
    public function getInviteCounter(): int
    {
        return (int)Redis::connection()->client()->get(self::TOTAL_INVITES_CACHE_PREFIX . $this->id);
    }

    public function incrementInviteCounter(bool $increment): void
    {
        if ($increment === true){
            Redis::connection()->client()->incr(self::TOTAL_INVITES_CACHE_PREFIX . $this->id);
        }

        if ($increment === false){
            Redis::connection()->client()->decr(self::TOTAL_INVITES_CACHE_PREFIX . $this->id);
        }
    }
}
