<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;


/**
 * Class Directory
 * @package App\Models
 * @method static create(array $array)
 * @method static where(string $string, $directoryUuid)
 */
class Directory extends Model
{
    /**
     * @return BelongsToMany
     */
    public function invites(): BelongsToMany
    {
        return $this
            ->belongsToMany(Invite::class, 'invites_shared_directories', 'directories_id', 'invites_id');
    }
    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this
            ->belongsToMany(User::class, 'users_directories', 'directories_id', 'users_id')
            ->withPivot('is_master');
    }

    /**
     * @return BelongsToMany
     */
    public function files(): BelongsToMany
    {
        return $this
            ->belongsToMany(File::class, 'directories_files', 'directories_id', 'files_id');
    }

    /**
     * @var string
     */
    protected $table = 'directories';
    /**
     * @var array
     */
    protected $fillable = ['name','uuid','created_at'];

    /**
     * @var bool
     */
    public $timestamps = false;

}
