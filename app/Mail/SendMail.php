<?php


namespace App\Mail;


use App\Models\User;
use Illuminate\Support\Facades\Mail;

/**
 * Class SendMail
 * @package App\Mail
 */
class SendMail
{

    /**
     * @var string
     */
    private $email;
    private $myEmail;

    /**
     * SendMail constructor.
     */
    public function __construct()
    {
        $config = config('exchanger');
        $this->myEmail = $config['my_email'];
    }
    /**
     * @param string $email
     */
    public function sendInviteMail(string $email): void
    {
        $this->email = $email;
        Mail::raw('The directories have been shared on you, register to view them: http://flyexchanger/api/v1/auth/register', function ($msg) {
            $msg->to($this->email);
            $msg->from($this->myEmail);
        });
    }

    /**
     * @param string $email
     * @param User $user
     */
    public function sendConfirmMail(string $email, User $user): void
    {
        $this->email = $email;
        Mail::raw('To confirm your account, go to: http://flyexchanger/api/v1/auth/confirm-register/'.$user->api_token, function ($msg) {
            $msg->to($this->email);
            $msg->from($this->myEmail);
        });
    }
}
