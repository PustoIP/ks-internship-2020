<?php


namespace App\Events;

use App\Models\Directory;
use App\Models\User;

/**
 * Class DirectorySharedEvent
 * @package App\Events
 */
class DirectorySharedEvent extends Event implements StatisticQueueEvent
{
    /* @var User */
    private $owner;
    /* @var User */
    private $invitee;
    /* @var Directory */
    private $directory;


    /**
     * DirectorySharedEvent constructor.
     * @param User $owner
     * @param User $invitee
     * @param Directory $directory
     */
    public function __construct(User $owner, User $invitee, Directory $directory )
    {
        $this->owner = $owner;
        $this->invitee = $invitee;
        $this->directory = $directory;
    }

    /**
     * @return array
     */
    public function getDataArray(): array
    {
        return [
            'owner_id' => $this->owner->id,
            'directory_uuid' => $this->directory->uuid,
            'invitee_email' => $this->invitee->email
        ];
    }
}
