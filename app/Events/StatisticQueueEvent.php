<?php


namespace App\Events;

/**
 * Interface StatisticQueueEvent
 * @package App\Events
 */
interface StatisticQueueEvent
{
    public function getDataArray(): array;

}
