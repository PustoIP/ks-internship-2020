<?php


namespace App\Events;

use App\Models\Directory;
use App\Models\File;
use App\Models\User;

/**
 * Class FileAddedEvent
 * @package App\Events
 */
class FileAddedEvent extends Event implements StatisticQueueEvent
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var Directory
     */
    private $directory;
    /**
     * @var File
     */
    private $file;


    /**
     * FileAddedEvent constructor.
     * @param User $user
     * @param Directory $directory
     * @param File $file
     */
    public function __construct(User $user, Directory $directory, File $file)
    {
        $this->user = $user;
        $this->directory = $directory;
        $this->file = $file;
    }

    /**
     * @return array
     */
    public function getDataArray(): array
    {
        return [
            'owner_id' => $this->user->id,
            'directory_uuid' => $this->directory->uuid,
            'file_uuid' => $this->file->uuid
        ];
    }
}
