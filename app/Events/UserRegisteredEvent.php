<?php


namespace App\Events;

use App\Models\User;

/**
 * Class UserRegisteredEvent
 * @package App\Events
 */
class UserRegisteredEvent extends Event implements StatisticQueueEvent
{
    /**
     * @var User
     */
    private $user;


    /**
     * UserRegisteredEvent constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function getDataArray(): array
    {
        return [
            'user_id' => $this->user->id
        ];
    }
}
