<?php


namespace App\Events;


use App\Models\User;

/**
 * Class UserInviteEvent
 * @package App\Events
 */
class UserInviteEvent extends Event implements StatisticQueueEvent
{

    /**
     * @var User
     */
    private $owner;
    /**
     * @var string
     */
    private $email;

    /**
     * FileAddedEvent constructor.
     * @param User $owner
     * @param string $email
     */
    public function __construct(User $owner, string $email)
    {
        $this->owner = $owner;
        $this->email = $email;
    }

    /**
     * @return array
     */
    public function getDataArray(): array
    {
        return [
            'inviter_id' => $this->owner->id,
            'invitee_email' => $this->email
        ];
    }
}
