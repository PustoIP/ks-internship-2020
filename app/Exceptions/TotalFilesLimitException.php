<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

/**
 * Class TotalFilesLimitException
 * @package App\Exceptions
 */
class TotalFilesLimitException extends BaseAppException
{
    /**
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
    protected $errorCode = ErrorCode::ACCOUNT_FILE_SIZE_LIMIT;
}
