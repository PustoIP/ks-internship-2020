<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;


/**
 * Class LifeTimeException
 * @package App\Exceptions
 */
class LifeTimeException extends BaseAppException
{
    /**
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
    protected $errorCode = ErrorCode::FILE_LIFE_TIME_ERROR;
}
