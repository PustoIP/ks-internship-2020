<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;


/**
 * Class DirectoriesException
 * @package App\Exceptions
 */
class DirectoriesException extends BaseAppException
{
    /**
     * @var int
     */
    protected $httpStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
    protected $errorCode = ErrorCode::DIRECTORY_ERROR;
}
