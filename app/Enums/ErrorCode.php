<?php
namespace App\Enums;
/**
 * Interface ErrorCode
 */
interface ErrorCode{

    public const REGISTER_FAIL = 1000;
    public const LOGIN_ERROR = 1001;
    public const INCORRECT_PASSWORD = 1002;
    public const USER_NOT_CONFIRM = 1003;
    public const NOT_HAVE_PERMISSION = 1004;

    public const DIRECTORY_ERROR = 2000;

    public const ACCOUNT_FILE_SIZE_LIMIT = 3000;
    public const FILE_LIFE_TIME_ERROR = 3001;

    public const INVITES_LIMIT = 4000;
}
