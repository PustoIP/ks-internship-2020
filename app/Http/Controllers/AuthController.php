<?php

namespace App\Http\Controllers;

use App\Events\UserRegisteredEvent;
use App\Exceptions\LoginException;
use App\Exceptions\NotHavePermissionException;
use App\Exceptions\PasswordException;
use App\Exceptions\RegistrationException;
use App\Mail\SendMail;
use App\Models\Directory;
use App\Models\Invite;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Throwable;


/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function register(Request $request): JsonResponse
    {
        $this->validate($request, [
            'password' => 'required|string',
            'email' => 'required|email|unique:users'
        ]);
        DB::beginTransaction();
        $email = $request->input('email');
        $password = Hash::make($request->input('password'));
        try {
            $user = User::create([
                'email' => $email,
                'user_password_hash' => $password,
                'api_token' => Str::random(45),
                'created_at' => Carbon::now()
            ]);

            $defaultDirectory = Directory::create([
                'name' => 'Default',
                'uuid' => Str::uuid(),
                'created_at' => Carbon::now()
            ]);


            if (!$user || !$defaultDirectory) {
                throw new RegistrationException('Register Fail!');
            }
            $user->directories()->attach($defaultDirectory, ['is_master' => true]);
            /* @var Invite $invites */
            $invites = Invite::query()->where('invite_email',$email)->get();
            /* @var Directory $directories */
            foreach ($invites as $invite){
                $directories = $invite->directories()->get();
                foreach ($directories as $directory){
                    $directory->users()->attach($user->id,['is_master' => false]);
                }
            }
            (new SendMail)->sendConfirmMail($email,$user);
            event(new UserRegisteredEvent($user));
        } catch (RegistrationException $e) {
            DB::rollBack();
        }
        DB::commit();
        /* @var User $user */
        return $this->successResponse($user);
    }

    /**
     * @param string $apiToken
     * @return JsonResponse
     * @throws NotHavePermissionException
     * @throws Throwable
     */
    public function confirmRegister(string $apiToken): JsonResponse
    {
        /* @var User $user */
        $user = Auth::user();
        if ($user->api_token !== $apiToken){
            throw new NotHavePermissionException('You cannot confirm this account because are not the owner!');
        }
        $user->confirmed = true;
        $user->saveOrFail();
        return $this->successResponse($user);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws LoginException
     */
    public function login(Request $request): JsonResponse
    {
        $this->validate($request, [
            'password' => 'required',
            'email' => 'required|email'
        ]);

        $login = User::where('email', $request->input('email'))->first();

        if (!$login) {
            throw new LoginException('Such user does not exist');
        }
        if (!Hash::check($request->input('password'), $login->user_password_hash)) {
            throw new LoginException('Incorrect password');
        }
        return $this->successResponse($login);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws PasswordException
     */
    public function updatePassword(Request $request): JsonResponse
    {
        $this->validate($request, [
            'api_token' => 'required',
            'old_password' => 'required',
            'new_password' => 'required'
        ]);
        $oldPassword = $request->input('old_password');
        $newPassword = $request->input('new_password');

        /* @var User $user */
        $user = Auth::user();

        if (!Hash::check($oldPassword, $user->user_password_hash)) {
            throw new PasswordException('Incorrect password!');
        }
        if (Hash::check($newPassword, $user->user_password_hash)) {
            throw new PasswordException('Old password and new password match!');
        }
        $user->user_password_hash = Hash::make($newPassword);
        $user->api_token = Str::random(45);
        $user->save();
        return $this->successResponse($user);
    }
}
