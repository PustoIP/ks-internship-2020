<?php


namespace App\Http\Controllers;

use App\Exceptions\InvitesLimitException;
use App\Exceptions\UserConfirmException;
use App\Models\Directory;
use App\Models\Invite;
use App\Models\User;
use App\Services\InvitesServices;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Throwable;

class InvitesController extends Controller
{
    /* @var User $user */
    private $user;
    /* @var InvitesServices $service */
    private $service;

    /**
     * InvitesController constructor.
     */
    public function __construct()
    {
        $this->service = new InvitesServices();
        $this->user = Auth::user();
    }

    /**
     * @param string $directoryUuid
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws InvitesLimitException
     * @throws UserConfirmException
     * @throws Throwable
     */
    public function shareAction(string $directoryUuid ,Request $request): JsonResponse
    {
        $this->validate($request, [
            'invite_email' => 'required|email'
        ]);
        $inviteEmail = $request->input('invite_email');
        /* @var Directory $directory */
        $directory = $this->user->directories()->where([['uuid', '=', $directoryUuid], ['is_master', '=', true]])->firstOrFail();
        $invite = $this->service->shareInvite($this->user,$directory,$inviteEmail);
        return $this->successResponse($invite);
    }

    /**
     * @param string $directoryUuid
     * @param int $inviteId
     * @return JsonResponse
     * @throws Exception
     */
    public function deleteAction(string $directoryUuid, int $inviteId): JsonResponse
    {
        /* @var Directory $directory */
        $directory = $this->user->directories()->where([['uuid', '=', $directoryUuid], ['is_master', '=', true]])->firstOrFail();
        /* @var Invite $invite */
        $invite = $directory->invites()->where('invites_id',$inviteId)->firstOrFail();
        $invite->delete();
        $this->service->downgradeInviteCounter($directory,$this->user);
        return $this->successResponse('Invitation successfully removed!');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws Exception
     */
    public function confirmAction(Request $request): JsonResponse
    {
        $this->validate($request,[
            'invite_id' => 'required',
            'confirm' => 'required|bool'
        ]);
        $inviteId = $request->input('invite_id');
        $confirm = (bool)$request->input('confirm');
        /* @var Invite $invite */
        $invite = Invite::query()->where('id',$inviteId)->firstOrFail();
        /* @var Directory $directory */
        $directory = $invite->directories()->where('invites_id',$inviteId)->firstOrFail();
        if ($confirm === true){
            $this->user->directories()->attach($directory->id,['is_master'=>false]);
            return $this->successResponse('The invitation has been successfully verified!');
        }
        $invite->delete();
        return $this->successResponse('The invitation was successfully been rejected!');
    }

    /**
     * @param string $directoryUuid
     * @return JsonResponse
     * @throws Exception
     */
    public function leaveAction(string $directoryUuid): JsonResponse
    {
        /* @var Directory $directory */
        $directory = $this->user->directories()->where([['uuid', '=', $directoryUuid], ['is_master', '=', false]])->firstOrFail();
        $invite = Invite::query()->where('invite_email',$this->user->email)->firstOrFail();
        $invite->delete();
        $this->user->directories()->detach($directory->id);
        /* @var User $master */
        $master = $directory->users()->where('is_master',true)->firstOrFail();
        $this->service->downgradeInviteCounter($directory,$master);
        return $this->successResponse('You have successfully left the directory!');
    }
}
