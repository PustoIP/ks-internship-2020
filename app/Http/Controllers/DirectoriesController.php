<?php


namespace App\Http\Controllers;

use App\Exceptions\DirectoriesException;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Directory;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;


/**
 * Class DirectoriesController
 * @package App\Http\Controllers
 */
class DirectoriesController extends Controller
{
    /* @var User $user */
    private $user;

    /**
     * DirectoriesController constructor.
     */
    public function __construct()
    {
        $this->user = Auth::user();
    }


    /**
     * @param Request $request
     * @return JsonResponse|void
     * @throws ValidationException
     */

    public function createAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);
        DB::beginTransaction();
        /* @var Directory $directory */
        try {
            $directory = Directory::create([
                'name' => $request->input('name'),
                'uuid' => Str::uuid(),
                'created_at' => Carbon::now()
            ]);
            if (!$directory) {
                throw new DirectoriesException('Directory not created!');
            }
            $this->user->directories()->attach($directory, ['is_master' => true]);
        } catch (DirectoriesException $e) {
            DB::rollback();
            return $e->render();
        }
        DB::commit();
        return $this->successResponse($directory);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function listAction(Request $request): JsonResponse
    {
        $this->validate($request, [
            'api_token' => 'required'
        ]);
        $directories = $this->user->directories;
        return response()->json([
            'success' => true,
            'directories' => $directories
        ], 200);
    }

    /**
     * @param string $directoryUuid
     * @return JsonResponse
     * @throws DirectoriesException
     */
    public function getAction(string $directoryUuid): JsonResponse
    {
        /* @var Directory $directory */
        $directory = $this->user->directories()->where('uuid', $directoryUuid)->first();

        if ($directory === null) {
            throw new DirectoriesException('You do not have permission to this directory!');
        }

        return $this->successResponse($directory);
    }

    /**
     * Update name of directory
     * @param $directoryUuid
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws DirectoriesException
     */
    public function updateAction(string $directoryUuid, Request $request): JsonResponse
    {
        $this->validate($request, [
            'new_name' => 'required|max:20'
        ]);
        /* @var Directory $directory */
        $directory = $this->user->directories()
            ->where([['uuid', '=', $directoryUuid], ['is_master', '=', true]])
            ->first();

        if ($directory === null) {
            throw new DirectoriesException('You do not have permission to this directory!');
        }
        $directory->name = $request->input('new_name');
        $directory->save();

        return $this->successResponse($directory);
    }
}
