<?php


namespace App\Http\Controllers;

use App\Exceptions\DirectoriesException;
use App\Exceptions\LifeTimeException;
use App\Models\Directory;
use App\Models\File;
use App\Models\User;
use App\Services\FilesServices;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Throwable;

/**
 * Class FilesController
 * @package App\Http\Controllers
 */
class FilesController extends Controller
{

    /* @var array $filesConfig */
    private $filesConfig;
    /* @var FilesServices $service */
    private $service;
    /* @var User $user */
    private $user;

    /**
     * FilesController constructor.
     */
    public function __construct()
    {
        $this->filesConfig = config('exchanger');
        $this->service = new FilesServices();
        $this->user = Auth::user();
    }


    /**
     * @param string $directoryUuid
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws Throwable
     */
    public function uploadAction(string $directoryUuid, Request $request): JsonResponse
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'lifetime_seconds' => [
                'int',
                'max:' . $this->filesConfig['file_max_lifetime_sec'],
                'min:' . $this->filesConfig['file_min_lifetime_sec']
            ],
            'file' => 'required|file|max:' . $this->filesConfig['file_max_size_kb']
        ]);
        /* @var Directory $directory */
        $directory = $this->user->directories()->where([['uuid', '=', $directoryUuid], ['is_master', '=', true]])->first();
        if ($directory === null) {
            throw new DirectoriesException('You do not have permission to this directory!');
        }
        $file = $request->file('file');
        $lifeTimeSeconds = $request->post('lifetime_seconds');
        $this->service->saveFile($this->user, $directory, $file, $request->post('name'), $lifeTimeSeconds);
        return $this->successResponse('File saved successfully!');
    }


    /**
     * @param string $directoryUuid
     * @return JsonResponse
     */
    public function listAction(string $directoryUuid): JsonResponse
    {
        /* @var Directory $directory */
        $directory = $this->user->directories()->where('uuid', $directoryUuid)->firstOrFail();
        /* @var File $files */
        $files = $directory->files()->get();
        return $this->successResponse($files);
    }

    /**
     * @param string $directoryUuid
     * @param string $fileUuid
     * @return JsonResponse
     * @throws Exception
     */
    public function deleteAction(string $directoryUuid, string $fileUuid): JsonResponse
    {
        /* @var Directory $directory */
        $directory = $this->user->directories()->where([['uuid', '=', $directoryUuid], ['is_master', '=', true]])->firstOrFail();
        /* @var File $file */
        $file = $directory->files()->where('uuid', $fileUuid)->firstOrFail();
        $this->user->downgradeFileSize($file->file_size);
        unlink($this->filesConfig['files_directory'] . $file->filename);
        $file->delete();

        return $this->successResponse('File success deleted');
    }


    /**
     * @param string $directoryUuid
     * @param string $fileUuid
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws LifeTimeException
     */
    public function editAction(string $directoryUuid, string $fileUuid, Request $request): ?JsonResponse
    {
        $this->validate($request, [
            'new_lifetime' => [
                'int',
                'max:' . $this->filesConfig['file_max_lifetime_sec'],
                'min:' . $this->filesConfig['file_min_lifetime_sec']
            ]
        ]);
        $newLifeTime = $request->input('new_lifetime');
        /* @var Directory $directory */
        $directory = $this->user->directories()->where([['uuid', '=', $directoryUuid], ['is_master', '=', true]])->firstOrFail();
        /* @var File $file */
        $file = $directory->files()->where('uuid', $fileUuid)->firstOrFail();
        $lifetime = time() - strtotime($file->created_at);
        $maxAvailableLifetime = $this->filesConfig['file_max_lifetime_sec'] - $lifetime;
        if ($newLifeTime < $maxAvailableLifetime) {
            $file->lifetime_seconds = $newLifeTime;
            $file->save();
            return $this->successResponse($file);
        }
        throw new LifeTimeException('The value (new_lifetime) can be set no more than ' . $maxAvailableLifetime . ' seconds');
    }


    /**
     * @param string $directoryUuid
     * @param string $fileUuid
     * @param Request $request
     * @throws ValidationException
     */
    public function moveAction(string $directoryUuid, string $fileUuid, Request $request): void
    {
        $this->validate($request, [
            'new_directory' => 'required'
        ]);
        $newDirectoryUuid = $request->input('new_directory');
        /* @var Directory $directory */
        $directory = $this->user->directories()->where([['uuid', '=', $directoryUuid], ['is_master', '=', true]])->firstOrFail();
        /* @var File $file */
        $file = $directory->files()->where('uuid', $fileUuid)->firstOrFail();
        $directory->files()->detach($file->id);
        /* @var Directory $newDirectory */
        $newDirectory = $this->user->directories()->where([['uuid', '=', $newDirectoryUuid], ['is_master', '=', true]])->firstOrFail();
        $newDirectory->files()->attach($file->id);
    }

    /**
     * @param string $directoryUuid
     * @param string $fileUuid
     * @param Request $request
     * @throws ValidationException
     */
    public function copyAction(string $directoryUuid, string $fileUuid, Request $request): void
    {
        $this->validate($request, [
            'new_directory' => 'required'
        ]);
        $newDirectoryUuid = $request->input('new_directory');
        /* @var Directory $directory */
        $directory = $this->user->directories()->where([['uuid', '=', $directoryUuid], ['is_master', '=', true]])->firstOrFail();
        /* @var File $file */
        $file = $directory->files()->where('uuid', $fileUuid)->firstOrFail();
        /* @var Directory $newDirectory */
        $newDirectory = $this->user->directories()->where([['uuid', '=', $newDirectoryUuid], ['is_master', '=', true]])->firstOrFail();
        $newDirectory->files()->attach($file->id);
    }


    /**
     * @param string $directoryUuid
     * @param string $fileUuid
     * @return BinaryFileResponse
     */
    public function downloadAction(string $directoryUuid, string $fileUuid): BinaryFileResponse
    {
        /* @var Directory $directory */
        $directory = $this->user->directories()->where('uuid', $directoryUuid)->firstOrFail();
        /* @var File $file */
        $file = $directory->files()->where('uuid', $fileUuid)->firstOrFail();
        return response()->download( $this->filesConfig['files_directory'] . $file->filename, 'Downloaded file');
    }
}
