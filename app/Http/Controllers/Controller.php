<?php

namespace App\Http\Controllers;


use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{

    /**
     * @param $user
     * @return JsonResponse
     */

    public function successResponse($user): JsonResponse
    {
        return response()->json([
            'success' => true,
            'data' => $user,
            'status code' => 200
        ], 200);
    }
}
