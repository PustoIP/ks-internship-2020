<?php

namespace App\Http\Middleware;

use App\Exceptions\UserConfirmException;
use App\Models\User;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth as AuthFacade;
use Illuminate\Contracts\Auth\Factory as Auth;

/**
 * Class Authenticate
 * @package App\Http\Middleware
 */
class Authenticate
{
    protected $errorCode = Response::HTTP_UNAUTHORIZED;
    /**
     * The authentication guard factory instance.
     *
     * @var Auth
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param Auth $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     * @throws UserConfirmException
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guard($guard)->guest()) {
            return response()->json([
                'success' => false,
                'message'=> 'Unauthorized user!'
            ], $this->errorCode);
        }
        $interval = config('confirmation_interval');
        /* @var User $user */
        $user = AuthFacade::user();
        if(!$user->confirmed && (strtotime($user->created_at) + $interval) < Carbon::now()->timestamp){
            throw new UserConfirmException('Confirm time expired!');
        }
        return $next($request);
    }
}
