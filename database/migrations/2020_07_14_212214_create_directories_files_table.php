<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDirectoriesFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directories_files', function (Blueprint $table) {
            $table->integer('directories_id')->unsigned();
            $table->integer('files_id')->unsigned();

            $table->foreign('files_id')
                ->references('id')->on('files')
                ->onDelete('cascade');

            $table->foreign('directories_id')
                ->references('id')->on('directories')
                ->onDelete('cascade');

            $table->unique(['directories_id','files_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('directories_files');
    }
}
