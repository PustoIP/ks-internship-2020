<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_directories', function (Blueprint $table) {
            $table->integer('users_id')->unsigned();
            $table->integer('directories_id')->unsigned();
            $table->boolean('is_master');

            $table->foreign('users_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('directories_id')
                ->references('id')->on('directories')
                ->onDelete('cascade');

            $table->unique(['users_id', 'directories_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_directories');
    }
}
