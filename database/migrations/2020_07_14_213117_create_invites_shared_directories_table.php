<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitesSharedDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invites_shared_directories', function (Blueprint $table) {
            $table->integer('invites_id')->unsigned();
            $table->integer('directories_id')->unsigned();

            $table->foreign('invites_id')
                ->references('id')->on('invites')
                ->onDelete('cascade');

            $table->foreign('directories_id')
                ->references('id')->on('directories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invites_shared_directories');
    }
}
