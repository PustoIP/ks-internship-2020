<?php
return [
    'file_min_lifetime_sec' => env('FILE_MIN_LT_SEC'),
    'file_max_lifetime_sec' => env('FILE_MAX_LT_SEC'),
    'file_max_size_kb' => env('FILE_MAX_SIZE'),
    'confirmed_account_max_size' => env('CONF_ACC_MAX_SIZE'),
    'not_confirmed_account_max_size' => env('NON_CONF_ACC_MAX_SIZE'),
    'my_email' => env('MAIL_USERNAME'),
    'confirmation_interval' => env('CONF_INTERVAL'),
    'files_directory' => '/var/www/FlyExchanger/uploadedFiles/',
    'max_uniq_shared_directories' => env('MAX_UNIQ_SHARED_DIR'),
];
